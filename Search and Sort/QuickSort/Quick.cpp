#include "Quick.h"

Quick::Quick()
{
}

Quick::~Quick()
{
}

void printArray(std::vector<int> arr, int size)
{
	int i;
	for (i = 0; i < size; i++)
		std::cout << arr[i] << ", ";
	std::cout << std::endl;
}

void Quick::QuickSort(std::vector<int>& vec, int low, int high)
{
	if (high > low)
	{
		int pivot = partition(vec, low, high);
		QuickSort(vec, low, pivot - 1);
		QuickSort(vec, pivot + 1, high);
		
	}
}

void Quick::swap(std::vector<int>& vec, int a, int b)
{
	int temp = vec[a];
	vec[a] = vec[b];
	vec[b] = temp;
}



// Based on Geek4Geek
int Quick::partition(std::vector<int>& vec, int low, int high)
{
	int pivot = vec[high]; // picks right most element to be the pivot
	int i = (low - 1);

	for (int j = low; j < high; j++) // goes from left to right 
	{
		if (vec[j] < pivot) // this swaps i with j for any elements smaller than element at j
		{
			i++;
			swap(vec, i, j);
		}
	}
	swap(vec, i + 1, high); // this swaps i with j where anything less than i is less than the piviot 
	return (i + 1);
}
