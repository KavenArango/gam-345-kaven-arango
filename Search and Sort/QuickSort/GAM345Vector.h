/*
* File name: GAM345
* FUNCTION: This file is a vector class that mimics the std vector class allowing for inserts removes pushbacks resizes and reserves
* Authour: Kaven Arango
* Date: 01/31/2021
* Main Resources used: https://upcoder.com/3/roll-your-own-vector
*/



#pragma once
#include <iostream>
#include <exception>
namespace GAM345
{






    template <typename T>
    class Vector
    {
    public:
        Vector();
        ~Vector();
        Vector(int);
        int capacity();
        int size();

        bool empty();

        void resize(int newVecSize);  // if smaller than n then will delete x elements
        T& operator[](int pos);


        void reserve(int newVecCap); // if smaller than n then nothing will happen
        void push_back(T newElement);
        void remove(int pos);
        void insert(T newElement, int pos);
        void clear(); // removes the elements


    private:
        int vecSize = 0, vecCapacity = 0;
        T* myVector = new T[vecSize];

        bool ValidElementAddition();
        bool validSize(int newVecSize);
        bool validCapacityChange(int newVecCap);
        void appendElementwithinVecCap(T* begin, T* end);

    };



    template<typename T>
    inline void Vector<T>::reserve(int newVecCap)
    {
        T* temp = (T*)(malloc(sizeof(T) * newVecCap));

        std::copy(myVector, myVector + vecSize, temp);


        std::free(myVector);

        myVector = temp;
        vecCapacity = newVecCap;
    }





    template<typename T>
    inline void Vector<T>::remove(int pos)
    {
        T* temp = (T*)(malloc(sizeof(T) * vecCapacity));
        std::copy(myVector, myVector + vecSize, temp);

        myVector = (T*)(malloc(sizeof(T) * vecCapacity));

        bool found = false;
        for (int i = 0; i <= vecSize; i++)
        {
            if (pos == i)
            {
                found = true; // found the element needed to be skipped
            }
            else
            {
                if (found)
                {
                    myVector[i - 1] = temp[i]; // shift the temp i into myvect -1 because of the skip over the found element
                }
                else
                {
                    myVector[i] = temp[i]; // copys until the element that needs to be removed is found
                }
            }
        }

        vecSize -= 1;
    }


















    template <typename T>
    void inline Vector<T>::push_back(T newelement)
    {
        if (false == ValidElementAddition()) // new element does not fit
        {
            reserve(vecSize + 1);
        }

        new((void*)(myVector + vecSize)) T(newelement); // creats a memory block and places the newelement into the block at secOne+pos
        ++vecSize;
    }


    template<typename T>
    inline void Vector<T>::insert(T newElement, int pos)
    {
        if (false == ValidElementAddition())
        {
            reserve(vecSize + 1);
        }

        T* secOne = (T*)(malloc(sizeof(T) * vecCapacity));
        std::copy(myVector, myVector + pos, secOne); // copys over all elements till the element that needs to be removed

        new((void*)(secOne + pos)) T(newElement); // creats a memory block and places the newelement into the block at secOne+pos

        T* secTwo = (T*)(malloc(sizeof(T) * vecCapacity));
        std::copy(myVector + pos, myVector + vecSize, secTwo); // from the element that needs to be removed to the end of the size


        free(myVector); // frees up myvector
        myVector = (T*)(malloc(sizeof(T) * vecCapacity)); // allocates memory the size of T

        for (int i = 0; i < vecSize; i++) // copys over the first section to myvector
        {
            myVector[i] = secOne[i];
        }

        for (int i = 0; i < vecSize; i++) // copys over the second section to my vector
        {
            myVector[i + pos + 1] = secTwo[i];
        }

        ++vecSize;
    }





    template<typename T>
    inline void Vector<T>::clear()
    {
        resize(0);
    }



    template<typename T>
    inline void Vector<T>::resize(int newVecSize)
    {

        if (newVecSize <= vecSize) // the new size is smaller than the old size
        {
            std::copy(myVector, myVector + newVecSize, myVector); // copys the elements
        }
        else
        {
            if (newVecSize >= vecCapacity) // builds a vector
            {
                T* temp = (T*)(malloc(sizeof(T) * newVecSize));
                std::copy(myVector, myVector + newVecSize, temp);
                free(myVector);
                myVector = temp;
            }
            // the new elements are biger than size but smaller than the cap
            appendElementwithinVecCap(myVector + vecSize, myVector + newVecSize); // calls the constructor for the elements that are needed
        }
        vecCapacity = newVecSize;
        vecSize = newVecSize;







        // might be needed not sure new code hasnt been tested enough
        //if (newVecSize <= vecSize) // the new size is smaller than the old size
        //{
        //    std::copy(myVector, myVector + newVecSize, myVector); // copys the elements
        //}
        //else
        //{
        //    if (newVecSize <= vecCapacity) // the new elements are biger than size but smaller than the cap
        //    {
        //        appendElementwithinVecCap(myVector + vecSize, myVector + newVecSize); // calls the constructor for the elements that are needed
        //    }
        //    else
        //    { // builds a vector
        //        T* temp = (T*)(malloc(sizeof(T) * newVecSize));
        //        std::copy(myVector, myVector + newVecSize, temp); 
        //        free(myVector);
        //        myVector = temp;
        //        appendElementwithinVecCap(myVector + vecSize, myVector + newVecSize);
        //    }
        //}
        //vecCapacity = newVecSize;
        //vecSize = newVecSize;
    }



    template<typename T>
    inline void Vector<T>::appendElementwithinVecCap(T* begin, T* end)
    {
        while (begin != end)
        {
            new((void*)begin) T(); // calls the contructor of T and adds it to the begin in my vector
            ++begin;
        }
    }


    template <typename T>
    T& Vector<T>::operator[](int index)
    {
        if (vecCapacity <= index)
        {
            throw "out of bounds";
        }

        return myVector[index];
    }


    template<typename T>
    inline Vector<T>::Vector()
    {
        vecCapacity = 0;
        vecSize = 0;
    }


    template<typename T>
    inline Vector<T>::~Vector()
    {
    }

    template<typename T>
    inline Vector<T>::Vector(int size)
    {
        reserve(10);
    }


    template<typename T>
    inline int Vector<T>::capacity()
    {
        return vecCapacity;
    }


    template<typename T>
    inline int Vector<T>::size()
    {
        return vecSize;
    }


    template<typename T>
    inline bool Vector<T>::empty()
    {
        if (vecSize == 0)
        {
            return true;
        }
        return false;
    }


    template<typename T>
    inline bool Vector<T>::ValidElementAddition()
    {
        if (vecSize == vecCapacity)
        {
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool Vector<T>::validSize(int newVecSize)
    {
        if (newVecSize <= 0)
        {
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool Vector<T>::validCapacityChange(int newVecCap)
    {
        if (vecSize > newVecCap)
        {
            return false;
        }
        return true;
    }




}