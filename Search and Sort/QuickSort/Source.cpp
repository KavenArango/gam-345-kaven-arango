#include <iostream>
#include <chrono>
#include <fstream>
#include <vector>
#include <algorithm>
#include <thread>
#include <future>
#include "GAM345Vector.h"
#include "Merge.h"
#include "Quick.h"

/*
* This code does not do what you want it to do it is no longer set up to use my vector class in any way shape or form while it did when 
* i collected the data in my pursite to try and speed up my merge sort i moved to cpp vector class to eliminate as many problems my poorly writen vector class
* has in this i was unable to do much if anything to speed up the merge sort and instead wasted hours if not days playing with the code 
* all i did was make a fool of myself and to help speed up run times i made it multitreaded waiting 5 minutes for code to run is boring
*/


void PrintArray(std::vector<int>& aArray, int aSize);
void CreateArray(std::vector<int>& aArray, int aSize);
bool SequentialSearch(std::vector<int>& aArray, int aSize, int aValue);


int TimeMergeSort(std::vector<int>& vec, int runs);
int TimeQuickSor(std::vector<int>& vec, int runs);
int cppSortTime(std::vector<int> Vec, int runs)
{
	auto startTimer = std::chrono::high_resolution_clock::now(); // start timer
	for (int i = 0; i < runs; i++)
	{
		CreateArray(Vec, Vec.size());
		std::sort(Vec.begin(), Vec.end());
	}
	auto stopTimer = std::chrono::high_resolution_clock::now(); // stops clock
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stopTimer - startTimer); // find the diffence from start time and stop time
	int Time = duration.count();
	return Time;
}


void write_csv(std::string filename, std::vector<std::pair<std::string, std::vector<int>>> dataset) {
	// Make a CSV file with one or more columns of integer values
	// Each column of data is represented by the pair <column name, column data>
	//   as std::pair<std::string, std::vector<int>>
	// The dataset is represented as a vector of these columns
	// Note that all columns should be the same size

	// Create an output filestream object
	std::ofstream myFile(filename);

	// Send column names to the stream
	for (int j = 0; j < dataset.size(); ++j)
	{
		myFile << dataset.at(j).first;
		if (j != dataset.size() - 1) myFile << ","; // No comma at end of line
	}
	myFile << "\n";

	// Send data to the stream
	for (int i = 0; i < dataset.at(0).second.size(); ++i)
	{
		for (int j = 0; j < dataset.size(); ++j)
		{
			myFile << dataset.at(j).second.at(i);
			if (j != dataset.size() - 1) myFile << ","; // No comma at end of line
		}
		myFile << "\n";
	}

	// Close the file
	myFile.close();
}


std::vector<int> cpptimeDatapoint(std::vector<int> Vec, std::vector<int>& result, int datapoints, int runs)
{
	std::vector<int> CPPTimes;
	for (int i = 0; i <= datapoints; i++)
	{
		result.push_back(cppSortTime(Vec, runs));
	}
	return CPPTimes;
}
std::vector<int> quicktimeDatapoint(std::vector<int> Vec, std::vector<int>& result, int datapoints, int runs)
{
	std::vector<int> QuickTimes;
	for (int i = 0; i <= datapoints; i++)
	{
		result.push_back(TimeQuickSor(Vec, runs));
	}

	return QuickTimes;
}
std::vector<int> mergetimeDatapoint(std::vector<int> Vec, std::vector<int>& result, int datapoints, int runs)
{
	std::vector<int> MergeTimes;
	for (int i = 0; i <= datapoints; i++)
	{
		result.push_back(TimeMergeSort(Vec, runs));
	}
	return MergeTimes;
}



int main()
{
	std::vector<int> Vec(10);
	std::vector<int> quick, merge, cpp;
	{

	//Merge myMerge;

	//std::cout << "Merge: " << std::endl;
	//CreateArray(Vec, Vec.size());
	//PrintArray(Vec, Vec.size());
	//myMerge.MergeSort(Vec, Vec.size()-1);
	//PrintArray(Vec, Vec.size());


	//Quick myQuick;

	//std::cout << "Quick: " << std::endl;
	//CreateArray(Vec, Vec.size());
	//PrintArray(Vec, Vec.size());
	//myQuick.QuickSort(Vec, 0, Vec.size() - 1);
	//PrintArray(Vec, Vec.size());
	}

	int runs = 1000;
	int datapoint = 300;
	
	std::cout << "Collecting Data Points" << std::endl;
	std::thread t1(quicktimeDatapoint, Vec, std::ref(quick), datapoint, runs);
	std::thread t2(mergetimeDatapoint, Vec, std::ref(merge), datapoint, runs);
	std::thread t3(cpptimeDatapoint, Vec, std::ref(cpp), datapoint, runs);
	

	t1.joinable();
	t1.join();

	t2.joinable();
	t2.join();

	t3.joinable();
	t3.join();

	std::cout << "Data Points Collected" << std::endl;
	std::vector<std::pair<std::string, std::vector<int>>> vals = {
		{"Quick", quick},
		{"Merge", merge},
		{"CPP", cpp}
	};
	write_csv("QuickVSMerge.csv", vals);
	std::cout << "Data Points Stored" << std::endl;
	return 0;
}



int TimeQuickSor(std::vector<int>& Vec, int runs)
{
	Quick myquick;
	auto startTimer = std::chrono::high_resolution_clock::now(); // start timer

	for (int i = 0; i < runs; i++)
	{
		CreateArray(Vec, Vec.size());
		myquick.QuickSort(Vec, 0, Vec.size() - 1);

	}

	auto stopTimer = std::chrono::high_resolution_clock::now(); // stops clock
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stopTimer - startTimer); // find the diffence from start time and stop time

	int quickTime = duration.count();
	return quickTime;
}

int TimeMergeSort(std::vector<int>& Vec, int runs)
{
	Merge myMerge;
	auto startTimer = std::chrono::high_resolution_clock::now(); // starts timer


	for (int i = 0; i < runs; i++)
	{
		CreateArray(Vec, Vec.size());
		myMerge.MergeSort(Vec, Vec.size() - 1);
	}



	auto stopTimer = std::chrono::high_resolution_clock::now(); // stops timer
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stopTimer - startTimer); // find the diffence from start time and stop time 

	int mergeTime = duration.count();
	return mergeTime;
}











void PrintArray(std::vector<int>& aArray, int aSize)
{
	for (int i = 0; i < aSize; i++)
	{
		std::cout << aArray[i];
		if (i < aSize - 1)// doesnt add a " , " at the end of the array
		{
			std::cout << ", ";
		}
	}
	std::cout << std::endl;
}

bool SequentialSearch(std::vector<int>& aArray, int aSize, int aValue)
{
	for (int i = 0; i < aSize; i++) // just a linear search
	{
		if (aValue == aArray[i])
		{
			return true;
		}
	}
	return false;
}

void CreateArray(std::vector<int>& aArray, int aSize)
{

	int i = 0, randomNumber;

	srand(time(0)); // seeds rand with time

	while (i < aSize) // fills the array
	{
		randomNumber = rand() % (aSize * 4); // gives a random number
		if (SequentialSearch(aArray, aSize, randomNumber) == false) // checks to make sure the number is unique
		{
			aArray[i] = randomNumber;
			i++;
		}
	}
}