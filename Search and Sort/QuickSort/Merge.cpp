#include "Merge.h"

Merge::Merge()
{
}

Merge::~Merge()
{
}

void Merge::MergeSort(std::vector<int>& a, int size)
{
	MergeSort(a, 0, size);
}

void Merge::MergeSort(std::vector<int>& a, int L, int R)
{
	if (L < R)
	{
		int m = (L + R) / 2;// finds the mid point

		merge(a, L, m, R); // this combines them
		MergeSort(a, L, m);// this does the left side of the mid point
		MergeSort(a, m + 1, R); // this works on the right side of the mid point
	}
}

void Merge::merge(std::vector<int>& a, int l, int m, int r)
{
	std::vector<int> temp; // stores the elements being sorted 

	int i = l;

	int j = m + 1;


	while (i <= m && j <= r) // incriments until either i or j reach m or r
	{
		if (a[i] <= a[j]) // checks if element at j is larger than element at i
		{
			temp.push_back(a[i]); // adds element at i to temp
			i++;
		}
		else
		{
			temp.push_back(a[j]); // adds element at j to temp
			j++;
		}
	}


	while (i <= m)// adds anything that  hasnt been sorted to temp
	{
		temp.push_back(a[i]);
		i++;
	}

	while (j <= r) // adds anything that  hasnt been sorted to temp
	{
		temp.push_back(a[j]);
		j++;
	}

	int w = temp.size();
	for (int i = 0; i < w; i++) // adds the elements back to the array sorted
	{
		a[l + i] = temp[i]; // starts at the end of the last elements that have been sorted so if elements 1-5 have been sorted then it will start after 5
		//a[l + i] = temp.front(); // starts at the end of the last elements that have been sorted so if elements 1-5 have been sorted then it will start after 5
		//temp.pop();
	}
}
