#pragma once
#include <iostream>
#include<vector>
#include <queue>



class Merge
{
public:
	Merge();
	~Merge();
	void MergeSort(std::vector<int>& a, int size);

private:
	void MergeSort(std::vector<int>& a, int L, int R);
	void merge(std::vector<int>& a, int L, int M, int R);
};

