#pragma once

#include <iostream>
#include <vector>


class Quick
{
public:
	Quick();
	~Quick();
	void QuickSort(std::vector<int>& vec, int low, int high);
private:
	void swap(std::vector<int>& vec, int a, int b);
	int partition(std::vector<int>& vec, int low, int high);
};

