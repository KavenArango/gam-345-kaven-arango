/*
* File name: Source
* FUNCTION: This file merge sorts and binary searches an array
* Authour: Kaven Arango
* Date: 03/07/2021
* Main Resources used: https://www.geeksforgeeks.org/merge-sort/
*/


#include <iostream>
#include <array> 
#include <vector> // for the temp in the merge otherwise id have to hard code a large size or make 2 classes
#include <ctime> // to seed rand
#include <random> // for rand
#include <queue>

using namespace std;

void MergeSort(int* a, int size);
void MergeSort(int* a, int L, int R); // splits up the main array recursevly 
void merge(int* a, int L, int M, int R); // sorts the array

int BinarySearch(int* a, int size, int value); // iterativly looks for the value

void PrintArray(int* aArray, int aSize);
void CreateArray(int* aArray, int aSize);
bool SequentialSearch(int* aArray, int aSize, int aValue);


void drivingcode(int* a, int size)
{
	int searchValue;
	cout << "Unsorted array" << endl;
	CreateArray(a, size);
	PrintArray(a, size);

	cout << "sorted array" << endl;
	MergeSort(a, size - 1);
	PrintArray(a, size);

	cout << "input a value to get" << endl;
	cin >> searchValue;
	int result = BinarySearch(a, size, searchValue);
	(result == -1) ? 
		cout << searchValue << " is not in the array" << endl << endl : 
		cout << searchValue <<" is at index " << result << endl << endl; // checks if the value returned is a -1
}

int main()
{
	const int aSize = 1, aSize1 = 5, aSize2 = 11;
	int aArray[aSize], aArray1[aSize1], aArray2[aSize2];
		//used to test all arrays
		//drivingcode(aArray, aSize);
		//drivingcode(aArray1, aSize1);
		drivingcode(aArray2, aSize2);

	return 0;
}

int BinarySearch(int* a, int size, int value)
{
	int l = 0, r = size, m; // Left side of the search right side and the middle
	while (l <= r)
	{
		m = (l + r) / 2; // finds the mid point
		if (a[m] == value) // value found
		{
			return m;
		}
		else
		{
			if (a[m] > value) // moves the r or l depending on if the mid point is smaller or larger than the value were looking for
			{
				r = m - 1;
			}
			else
			{
				l = m + 1;
			}
		}
	}
	return -1;
}

void MergeSort(int* a, int L, int R)
{
	if (L < R)
	{
		int m = (L + R) / 2;// finds the mid point

		MergeSort(a, L, m);// this does the left side of the mid point
		MergeSort(a, m + 1, R); // this works on the right side of the mid point
		merge(a, L, m, R); // this combines them
	}
}










void merge(int* a, int l, int m, int r)
{
	queue<int> temp; // stores the elements being sorted 

	int i = l;

	int j = m + 1;


	while (i <= m && j <= r) // incriments until either i or j reach m or r
	{
		if (a[i] <= a[j]) // checks if element at j is larger than element at i
		{
			temp.push(a[i]); // adds element at i to temp
			i++;
		}
		else
		{
			temp.push(a[j]); // adds element at j to temp
			j++;
		}
	}


	while (i <= m)// adds anything that  hasnt been sorted to temp
	{
		temp.push(a[i]);
		i++;
	}

	while (j <= r) // adds anything that  hasnt been sorted to temp
	{
		temp.push(a[j]);
		j++;
	}

	int w = temp.size();
	for (int i = 0; i < w; i++) // adds the elements back to the array sorted
	{
		a[l + i] = temp.front(); // starts at the end of the last elements that have been sorted so if elements 1-5 have been sorted then it will start after 5
		temp.pop();
	}
}

























//void merge(int* a, int l, int m, int r)
//{
//	vector<int> temp; // stores the elements being sorted 
//
//	int i = l;
//
//	int j = m + 1;
//
//
//	while (i <= m && j <= r) // incriments until either i or j reach m or r
//	{
//		if (a[i] <= a[j]) // checks if element at j is larger than element at i
//		{
//			temp.push_back(a[i]); // adds element at i to temp
//			i++;
//		}
//		else
//		{
//			temp.push_back(a[j]); // adds element at j to temp
//			j++;
//		}
//	}
//
//
//	while (i <= m)// adds anything that  hasnt been sorted to temp
//	{
//		temp.push_back(a[i]);
//		i++;
//	}
//
//	while (j <= r) // adds anything that  hasnt been sorted to temp
//	{
//		temp.push_back(a[j]);
//		j++;
//	}
//
//	for (int i = 0; i < temp.size(); i++) // adds the elements back to the array sorted
//	{
//		a[l+i] = temp[i]; // starts at the end of the last elements that have been sorted so if elements 1-5 have been sorted then it will start after 5
//	}
//}

void MergeSort(int* a, int size)
{
	//Left needs to be a variable otherwise this code no longer works ;-;
	MergeSort(a, 0, size);
}

void PrintArray(int* aArray, int aSize)
{
	for (int i = 0; i < aSize; i++)
	{
		cout << aArray[i];
		if (i < aSize - 1)// doesnt add a " , " at the end of the array
		{
			cout << ", ";
		}
	}
	cout << endl;
}


void CreateArray(int* aArray, int aSize)
{

	int i = 0, randomNumber;

	srand(time(0)); // seeds rand with time

	while (i < aSize) // fills the array
	{
		randomNumber = rand() % (aSize * 4); // gives a random number
		if (SequentialSearch(aArray, aSize, randomNumber) == false) // checks to make sure the number is unique
		{
			aArray[i] = randomNumber;
			i++;
		}
	}
}

bool SequentialSearch(int* aArray, int aSize, int aValue)
{
	for (int i = 0; i < aSize; i++) // just a linear search
	{
		if (aValue == aArray[i])
		{
			return true;
		}
	}
	return false;
}