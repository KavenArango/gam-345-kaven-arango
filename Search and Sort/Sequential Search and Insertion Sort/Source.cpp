/*
* File name: Source
* FUNCTION: This file is the whole program it has Insertion Sort and Sequential Search
* Authour: Kaven Arango
* Date: 02/26/2021
* Main Resources used: none
*/



#include <iostream>
#include <array>
#include <ctime> // to seed rand
#include <random> // for rand

using namespace std;
bool SequentialSearch(int* aArray, int aSize, int aValue);
void InsertionSort(int* aArray, int aSize);
void PrintArray(int* aArray, int aSize);
void CreateArray(int* aArray, int aSize);
void ManualTest(int* aArray, int aSize);
void AutoTest(int* aArray, int aSize);

int main()
{
	const int aSize = 10;
	int aArray[aSize];
	int userChoice;
	
	bool keeprunning = true;

	while (keeprunning == true)
	{
		cout << "1) Auto Testing" << endl;
		cout << "2) Manual Testing" << endl;
		cout << "3) Exit" << endl;

		cin >> userChoice;

		switch (userChoice)
		{
		case 1:
		{
			AutoTest(aArray, aSize);
			break;
		}
		case 2:
		{
			ManualTest(aArray, aSize);
			break;
		}
		case 3:
		{
			cout << "Have a nice day!" << endl;
			keeprunning = false;
			break;
		}
		default:
		{
			cout << "Invalid input try again" << endl;
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); // Cleans up the cin buffer
			userChoice = 0;
			break;
		}
		}
	}
	return 0;
}


void InsertionSort(int* aArray, int aSize)
{
	int target;
	for (int i = 1; i < aSize; i++)
	{
		target = aArray[i]; //target is the element being moved to the right we start at element 1 because there is nothing to the left of element 0 therefor it is either correct or doesnt need to be looked at for the first run
		
		for (int j = i-1; j >= 0; j--)// starts the look at one element to the left of target
		{
			if (aArray[j] >= target) // if the element to the left of target is higher value then it shifts the element to the right
			{
				aArray[j + 1] = aArray[j]; // shifting the element
				aArray[j] = target; // places the target in the new spot while this isnt exactly what was asked it still comes out to be what we want otherwise there would be repeats
			}
		}
	}
}



bool SequentialSearch(int* aArray, int aSize, int aValue)
{
	for (int i = 0; i < aSize; i++) // just a linear search
	{
		if (aValue == aArray[i])
		{
			return true;
		}
	}
	return false;
}

void PrintArray(int* aArray, int aSize)
{
	for (int i = 0; i < aSize; i++)
	{
		cout << aArray[i];
		if (i < aSize - 1)// doesnt add a " , " at the end of the array
		{
			cout << ", ";
		}
	}
	cout << endl;
}

void CreateArray(int* aArray, int aSize)
{
	
	int i = 0, randomNumber;

	srand(time(0)); // seeds rand with time

	while (i < aSize) // fills the array
	{
		randomNumber = rand() % (aSize * 4); // gives a random number
		if (SequentialSearch(aArray, aSize, randomNumber) == false) // checks to make sure the number is unique
		{
			aArray[i] = randomNumber;
			i++;
		}
	}
}

void ManualTest(int* aArray, int aSize)
{
	int userChoice;
	bool keeprunning = true;
	while (keeprunning == true)
	{
		cout << "Array:" << endl;
		PrintArray(aArray, aSize);

		cout << "1) Sequential Search" << endl;
		cout << "2) Insertion Sort" << endl;
		cout << "3) Create Array" << endl;

		cout << "4) Exit" << endl;
		cin >> userChoice;

		switch (userChoice)
		{
		case 1:
		{
			cout << "What Element would you like to find?" << endl;
			cin >> userChoice;
			if (cin.fail())
			{
				cout << "Invalid input" << endl;
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n'); // cleans the cin buffer
			}
			else
			{
				if (SequentialSearch(aArray, aSize, userChoice))
				{
					cout << "Element exists" << endl;
				}
				else
				{
					cout << "Element does not exists" << endl;
				}
			}

			break;
		}
		case 2:
		{
			InsertionSort(aArray, aSize);
			break;
		}
		case 3:
		{
			CreateArray(aArray, aSize);
			break;
		}
		case 4:
		{
			keeprunning = false;
			break;
		}
		default:
		{
			cout << "Invalid input try again" << endl;
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); // cleans the cin buffer

			userChoice = 0;
			break;
		}
		}
	}
}

void AutoTest(int* aArray, int aSize)
{
	cout << "Empty Array:" << endl;
	PrintArray(aArray, aSize);
	cout << "Searching for Element of value 5" << endl;
	if (SequentialSearch(aArray, aSize, 5))
	{
		cout << "Element has been found" << endl;
	}
	else
	{
		cout << "Element has not been found" << endl;
	}
	cout << "Sorting Array" << endl;
	InsertionSort(aArray, aSize);
	PrintArray(aArray, aSize);
	cout << endl << endl;






	cout << "Unsorted Array:" << endl;
	CreateArray(aArray, aSize);
	PrintArray(aArray, aSize);
	cout << "Searching for Element of value 5" << endl;
	if (SequentialSearch(aArray, aSize, 5))
	{
		cout << "Element has been found" << endl;
	}
	else
	{
		cout << "Element has not been found" << endl;
	}
	cout << "Sorting Array" << endl;
	InsertionSort(aArray, aSize);
	PrintArray(aArray, aSize);
	cout << endl << endl;


	cout << "Sorted Array:" << endl;
	PrintArray(aArray, aSize);
	cout << "Searching for Element of value 5" << endl;
	if (SequentialSearch(aArray, aSize, 5))
	{
		cout << "Element has been found" << endl;
	}
	else
	{
		cout << "Element has not been found" << endl;
	}
	cout << "Sorting Array" << endl;
	InsertionSort(aArray, aSize);
	PrintArray(aArray, aSize);
	cout << endl << endl;






	cout << "Reverse Array:" << endl;
	for (int i = 0; i < 10; i++)
	{
		aArray[aSize - i-1] = i;
	}
	PrintArray(aArray, aSize);
	cout << "Searching for Element of value 5" << endl;
	if (SequentialSearch(aArray, aSize, 5))
	{
		cout << "Element has been found" << endl;
	}
	else
	{
		cout << "Element has not been found" << endl;
	}
	cout << "Sorting Array" << endl;
	InsertionSort(aArray, aSize);
	PrintArray(aArray, aSize);
	cout << endl << endl;
}
