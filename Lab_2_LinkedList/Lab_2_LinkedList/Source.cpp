/*
* File name: Source
* FUNCTION: This file is the driver code used to test the linked list and try to insure everything is working correctly
* Authour: Kaven Arango
* Date: 02/14/2021
* Main Resources used: none
*/


#include <iostream>
#include "GAM345LinkedList.h"

class TestElement
{
public:
	TestElement() { elementId = ++lastElementId; }
	~TestElement()
	{}
	int elementId;
private:
	static int lastElementId;
};
int TestElement::lastElementId = 0;

int main()
{
	GAM345::LinkedList<TestElement> test;

	for (int i = 0; i < 10; i++)
	{
		test.insert(TestElement(), i);
	}



	std::cout << "Inserted 10 elements into the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	for (int i = 0; i < test.size(); i++)
	{
		test[i].elementId++;
	}

	std::cout << std::endl << std::endl;
	std::cout << "adding 1 to all elements in the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;


	test.insert(TestElement(), 20);
	std::cout << "Inserting past linked list size: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.insert(TestElement(), 0);
	std::cout << "Inserting at head of linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.insert(TestElement(), test.size());
	std::cout << "Inserting at end of linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.insert(TestElement(), (test.size() / 2));
	std::cout << "Inserting in the middle of the linked list size: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;




	test.erase(0);
	std::cout << "Removing head of the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.erase(test.size());
	std::cout << "Removing end of the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.erase((test.size() / 2));
	std::cout << "Removing the middle of the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;



	test.erase(2);
	std::cout << "Removing the second element of the linked list: " << std::endl;
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}
	std::cout << std::endl << std::endl;




	std::cout << "Testing access to elements" << std::endl;
	std::cout << "Element at position 5: " << test[5].elementId << std::endl;
	std::cout << std::endl;
	std::cout << "Testing size call" << std::endl;
	std::cout << "LinkedList size: " << test.size() << std::endl;
	std::cout << std::endl << std::endl;





	std::cout << "Clearing linkedlist:" << std::endl;
	test.clear();
	for (int i = 0; i < test.size(); i++)
	{
		std::cout << "Element " << i << ": " << test[i].elementId << std::endl;
	}


	return 0;
}