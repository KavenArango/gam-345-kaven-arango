/*
* File name: GAM345LinkedList
* FUNCTION: This file is a LinkedList class thats allows the user to insert into the list as well as remove and clear all the elements using any data structor the user wants
* Authour: Kaven Arango
* Date: 02/14/2021
* Main Resources used: none
*/




#pragma once

#include <exception>
#include "ElementWrapper.h"



namespace GAM345
{
	template <typename T>
	class LinkedList
	{
	public:
		LinkedList();
		~LinkedList();
		void insert(T newElement, int pos);
		int size();
		T& operator[](int pos);
		void erase(int pos);
		void clear();

	private:
		void remFirst(); // remove first element
		void append(ElementWrapper<T>* wrapper); // adds to the back of the list
		void elementSwapper(ElementWrapper<T>* newWrapper, int pos); // inserts the new element in the list as long as its not the first element
		void headSwapper(ElementWrapper<T>* newWrapper); // inserts the new element to the front
		bool checkIfNull(ElementWrapper<T>* element); // checks if the next element is null only really used for the head after some refactoring was done
		void ValidAddition(int pos); // checks if the new element can be added
		void ValidRemoval(int pos); // checks if the element can be removed
		int listSize;


		ElementWrapper<T>* head;
		ElementWrapper<T>* tail;
		ElementWrapper<T>* previousElement; // test elements after a refactor their names became meaningless also used to try and avoid mem leaks
		ElementWrapper<T>* nextElement; // also a test element
	};



	template<typename T>
	inline void LinkedList<T>::ValidAddition(int pos)
	{
		if (pos < 0) // position of the new element is less than 0
		{
			throw "Invalid List postion cannot be less than 0";
		}
	}


	template<typename T>
	inline void LinkedList<T>::ValidRemoval(int pos)
	{
		ValidAddition(pos);
		if (pos > listSize) // position of the removal element is greater than the list size
		{
			throw "Invalid List position cannot be greater than List size";
		}
	}


















	template<typename T>
	inline void LinkedList<T>::insert(T newElement, int pos)
	{
		ValidAddition(pos);

		ElementWrapper<T>* wrapper = new ElementWrapper<T>(newElement);
		if (checkIfNull(head) == true) // head is null and needs to be assigned a variable
		{
			head = wrapper;
			tail = head;
		}
		else
		{
			if (pos >= listSize)//element is past the length of the linkedlist
			{
				append(wrapper); // adds to the back of the linked list
			}
			else
			{
				if (pos == 0) //if the head needs to be swapped
				{
					headSwapper(wrapper);
				}
				else
				{
					elementSwapper(wrapper, pos); // swaps any other elements that need to be swapped
				}
			}
		}
		wrapper = nullptr; // removes the link to the newly added element
		delete wrapper; // removes the mem
		wrapper = nullptr; // clears the wrapper
		listSize++;
	}


	template<typename T>
	inline void LinkedList<T>::erase(int pos)
	{
		ValidRemoval(pos);
		nextElement = nullptr; // safty thing to make sure that next element is empty before starting to use it i thought about moving it in here but []operation gives a potential mem leak

		if (pos == 0)
		{
			remFirst();
		}
		else
		{
			nextElement = head;
			for (int i = 0; i < pos-2; i++) // finds the element before the element that needs to be removed
			{
				nextElement = nextElement->getNextWrapper();
			}
			previousElement = nextElement->getNextWrapper(); // begins the swap
			nextElement->setNextWrapper(previousElement->getNextWrapper());
			previousElement->setNextWrapper(nullptr);
			delete previousElement;
			previousElement = nullptr;
			
		}
		listSize--;
	}


	template<typename T>
	inline void LinkedList<T>::remFirst()
	{
		nextElement = nullptr;
		nextElement = head;
		head = nextElement->getNextWrapper();

		delete nextElement;
		nextElement = nullptr;
	}


	template<typename T>
	inline void LinkedList<T>::clear()
	{
		
		tail = nullptr;
		previousElement = nullptr;
		nextElement = nullptr;
		
		head->~ElementWrapper();
		head = nullptr;
		listSize = 0;
	}


	template<typename T>
	inline void LinkedList<T>::elementSwapper(ElementWrapper<T>* newWrapper, int pos)
	{
		nextElement = nullptr;
		nextElement = head;

		for (int i = 0; i < pos-1; i++) // finds the element before the element that needs to be removed
		{
			nextElement = nextElement->getNextWrapper();
		}
		newWrapper->setNextWrapper(nextElement->getNextWrapper());
		nextElement->setNextWrapper(nullptr);
		nextElement->setNextWrapper(newWrapper);
	}


	template<typename T>
	inline void LinkedList<T>::headSwapper(ElementWrapper<T>* newWrapper)
	{
		nextElement = nullptr;
		nextElement = head;
		head = nullptr;
		
		head = newWrapper;
		head->setNextWrapper(nextElement);
		nextElement = nullptr;
	}


	template<typename T>
	inline bool LinkedList<T>::checkIfNull(ElementWrapper<T>* element)
	{
		if (element == nullptr)
		{
			return true;
		}
		return false;
	}


	template<typename T>
	inline void LinkedList<T>::append(ElementWrapper<T>* wrapper)
	{
		tail->setNextWrapper(wrapper);
		tail = tail->getNextWrapper();
		wrapper = nullptr;
	}


	template<typename T>
	inline T& LinkedList<T>::operator[](int pos)
	{
		if (pos >= listSize)
		{
			throw "out of bounds";
		}
		nextElement = nullptr;
		nextElement = head;
		for (int i = 0; i < pos; i++)
		{
			nextElement = nextElement->getNextWrapper();
		}
		return nextElement->getElement();
	}


	template<typename T>
	inline int LinkedList<T>::size()
	{
		return listSize;
	}


	template<typename T>
	inline LinkedList<T>::LinkedList()
	{
		listSize = 0;
		head = nullptr;
		nextElement = nullptr;
		previousElement = nullptr;
	}


	template<typename T>
	inline LinkedList<T>::~LinkedList()
	{
	}
}