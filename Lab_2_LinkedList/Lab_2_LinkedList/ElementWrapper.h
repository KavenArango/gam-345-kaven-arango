/*
* File name: ElementWrapper
* FUNCTION: This file is a Wrapper class  used by the linkedlist that allows the linkedlist to know what is the next element in the list as well as storing the data that should be in the linked list at a certian index
* Authour: Kaven Arango
* Date: 02/14/2021
* Main Resources used: none
*/


#pragma once

	template<typename T>
	class ElementWrapper
	{
	public:
		ElementWrapper();
		ElementWrapper(T newData);
		~ElementWrapper();

		ElementWrapper<T>* getNextWrapper();
		void setNextWrapper(ElementWrapper<T>* newWapper);

		T& getElement();
		void setElement(T val);

	private:
		T* Data = new T[0];
		ElementWrapper<T>* nextWrapper;

	};


	template<typename T>
	inline ElementWrapper<T>::ElementWrapper()
	{
		Data = nullptr;
		nextWrapper = nullptr;
	}

	template<typename T>
	inline ElementWrapper<T>::ElementWrapper(T newData)
	{
		new((void*)(Data)) T(newData); // adding new data to the data block i dont know why this works it shouldnt work i testing it several times with 150000 elements and it works SOS
		nextWrapper = nullptr;
	}

	template<typename T>
	ElementWrapper<T>::~ElementWrapper()
	{
	}

	template<typename T>
	ElementWrapper<T>* ElementWrapper<T>::getNextWrapper()
	{
		return nextWrapper;
	}



	template<typename T>
	inline void ElementWrapper<T>::setNextWrapper(ElementWrapper<T>* newWapper)
	{
		nextWrapper = newWapper;
	}

	template<typename T>
	inline T& ElementWrapper<T>::getElement()
	{
		return *(Data); // again unsure why this works but it works and it works as expected so im not going to complain also returns Data
	}

	template<typename T>
	inline void ElementWrapper<T>::setElement(T val)
	{
		Data = val;
	}


