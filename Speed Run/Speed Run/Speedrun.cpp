/*
* File name: Speedrun.cpp
* FUNCTION: This codes function is to if the input string has duplicate numbers and if they have a number the user wants to search for
* this code is built for speed and nothing else 
* Authour: Kaven Arango
* Date: 04/10/2021
* Main Resources used: none
* Comment: This code was writen to simply make it as fast as i could. No one should ever code like this take the milliseconds and make the code readable
*/
#include "Speedrun.h"


void Speedrun::results(bool& repeatingNum, bool& numFound)
{
	if (numFound == true) // test system is case sensitive 
	{
		printf("True\n"); // used to try and make the system faster
	}
	else
	{
		printf("False\n");
	}
	if (repeatingNum == true)
	{
		printf("True\n");
	}
	else
	{
		printf("False\n");
	}
}

// takes the line input and parses out the last number this will also reverse anything given to it
// also my attempt at making the code faster proved to be imeasurable
//string Speedrun::parser(string& line) 
//{
//	string temp = "", num = ""; // temp is used as a buffer to keep spaces out of the return value
//	int size = line.size() - 1; // stores the size of the line being given
//	while (size != 0)
//	{
//		if (temp == " " && num == "")
//		{
//			temp = "";
//			continue;
//		}
//		else
//		{
//			if (temp == " ")
//			{
//				break;
//			}
//			num += temp;
//			temp = line.back();
//			line.pop_back();
//			--size;
//		}
//	}
//	num += temp; // ensures nothing is left in temp once the function is done
//	return num;
//}

// does the searching its done in here because once its happens this code will 
// not function since parts of the array may be thrown out for the sake of speed
// I havge no idea if my code is faster but i really hope so or im not going to be happy at spending 4 daying writing this
Speedrun::Speedrun(string& Ssize, string& line, string& Ssearching) 
{ // hash map might have been a good idea but the only way i found to make one was slow to construct but at this point it might have been faster who knows
	bool repeatingNum = false, numFound = false; // keeps track of what the output should be
	int value = 0, size = stoi(Ssize) * 2, searching = stoi(Ssearching);
	//int value = 0, size = stoi(Ssize) * 2, searching = stoi((parser(Ssearching))+Ssearching.front()); // convers the params to ints size is being multiplied by two to try and avoid colissions
	int pos1; // used to store the expected position in the array
	const int secondarraysize = 11; // some random number i thought of to try and help avoid collisions 
	int *numbers = new int[size * 11]; // makes a very large array with tons of empty spots that are prolly never going to be used
	stringstream ss(line);
	
	//while (line.size()-1 != 0)
	while (getline(ss, line, ' '))
	{
		//value = stoi(parser(line)); // gets the next number from the line we want to look at again this number will be backwards
		value = stoi((line));
		if (value == searching) // checks if the numbers are what we are looking for
		{
			numFound = true; 
			if (repeatingNum) // if we found a repeating number this will cut the while loop short to print the output
			{
				break;
			}
		}
		if (repeatingNum == false) // checks if we found the repeating number this is a lot of code and if it can be avoided it would be best to do so
		{
			pos1 = (value % size) + (value % secondarraysize); // calculates the position of a element in the array that way we can index staight to it
			if (numbers[pos1] == value) // checks if the value at the position is repeating
			{
				repeatingNum = true;
				if (numFound) // cuts the loop short since we no long need to search
				{
					break;
				}
			}
			// adds the new number to the array it may overwrite the prevoius number if there is a collision but adding an IF would be 
			// ever so slightly slower
			numbers[pos1] = value; 
		}
	}
	// prints the results it was tested to see if removing this changes the speed its inmeasurable
	results(repeatingNum, numFound); 
}