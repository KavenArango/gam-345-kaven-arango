/*
* File name: Speedrun.h
* FUNCTION: This codes function is to if the input string has duplicate numbers and if they have a number the user wants to search for
* this code is built for speed and nothing else
* Authour: Kaven Arango
* Date: 04/10/2021
* Main Resources used: none
*/
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

class Speedrun
{
public:
	Speedrun(string& size, string& numbers, string& searching); // does all the searching
	void results(bool&, bool&); // prints the result of the searching
	//string parser(string& line); // used to parse out numbers needed for the searching
};