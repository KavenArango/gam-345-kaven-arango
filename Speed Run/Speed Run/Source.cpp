/*
* File name: main.cpp
* FUNCTION: This code opens and reads from a file
* Authour: Kaven Arango
* Date: 04/10/2021
* Main Resources used: main provided in the assignment to read the file
* Comment: Do not read this code i have a headache trying to write it god forbid someone tried to read it
*/
#include "Speedrun.h"


int main() //code is only good for one run and will not function for any thing else 
{
	string line, ssize, ssearchfor; // for the 3 lines from the file
	ifstream myfile("Numbers.txt"); // reads the file fail check was removed to try and speed the system up
	getline(myfile, ssize); // reads the size of the array
	getline(myfile, line); // gets all the numbers that belong in the array
	getline(myfile, ssearchfor); // gets the number we are searching for from the file
	Speedrun speed = Speedrun(ssize, line, ssearchfor); // makes an object that will read the list of numbers
	return 0;
}