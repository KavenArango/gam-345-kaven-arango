/*
* File name: Graph.cpp
* FUNCTION: This builds the path from one node to the next in the graph as well as adding new nodes to the graph 
* Authour: Kaven Arango
* Date: 04/24/2021
* Main Resources used: none
*/



#include "Graph.h"
#include <string>
#include <vector>
#include <queue>
#include <map>

Graph::Graph()
{
}

Graph::~Graph()
{
}

void Graph::AddNode(char aNodeId, int aNumConnections, const char* aConnectedNodeList)
{
	nodes.push_back(Node(aNodeId, aNumConnections, aConnectedNodeList));
}

bool Graph::GetNode(char aNodeId, Node& aNode)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i].nodeId == aNodeId)
		{
			aNode = nodes[i];
			return true;

		}
	}
	return false;
}

char* Graph::FindPath(const char aStartId, const char aEndID)
{
	bool nodeVisited = false; // tracks if the node were looking at has been visited
	std::queue<char> nodesInQueue; // tracks the nodes we havent looked at yet
	std::vector<char> visited; // the nodes we have looked at
	Node n; // calls the node 
	std::string path = ""; // the path we are using
	nodesInQueue.push(aStartId); // pushes the start into the nodes we need to check
	visited.push_back(aStartId); // pushes the nodes because we are currently looking at it
	
	
	
	while (false == nodesInQueue.empty()) // keeps looking until the queue is empty
	{
		GetNode(nodesInQueue.front(), n); // gets the node in the queue
		path += n.nodeId; // adds the node ID into the path

		if (n.nodeId == aEndID) // checks if the current node is the node were looking for
		{
			break;
		}
		path += ", "; // makes it look pretty

		for (int i = 0; i < n.numConnections; i++) // we need to look over all the connections
		{
			for (int j = 0; j < visited.size(); j++) // we need to see if that node has been visited
			{
				if (n.connections[i] == visited[j])
				{
					nodeVisited = true;
				}
			}
			if (nodeVisited == false)
			{
				nodesInQueue.push(n.connections[i]); // pushes the new nodes into the queue
				visited.push_back(n.connections[i]); // adds it to visited so it doesnt get pushed again
			}
			nodeVisited = false;
		}
		nodesInQueue.pop(); // removes the front element
	}

	if (path[path.length()-1] != aEndID) // checks if the path was made
	{
		path = "No Path";
	}

	char* cstr = new char[path.length() + 1]; // formating for the return
	strcpy_s(cstr, path.length() + 1, path.c_str());

	return cstr;
}

