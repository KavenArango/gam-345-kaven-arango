#pragma once
// A node within this Graph class
// Each node is represented by a single letter
struct Node
{
	// Constructors
	Node();
	Node(char aNodeId, int aNumConnections, const char* aNodeConnectionList);

	// The id of this node, represented by a single letter
	char nodeId;

	// The number of connections this node has
	int numConnections;

	// The other nodes that this node is connected to, represented by a character array
	const char* connections;
};
