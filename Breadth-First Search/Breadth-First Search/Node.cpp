#include "Node.h"

Node::Node() :
	nodeId('-'), numConnections(0), connections(nullptr)
{

}

Node::Node(char aNodeId, int aNumConnections, const char* aNodeConnectionList):
	nodeId(aNodeId), numConnections(aNumConnections), connections(aNodeConnectionList)
{

}
