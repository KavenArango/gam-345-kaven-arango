#include "BinaryTree.h"
/*
* File name: BinaryTree
* FUNCTION: This is the sort binary tree this tree does not self balance and will only insert where the node is fit to go
* This tree can insert, look up nodes and print inorder
* Authour: Kaven Arango
* Date: 03/31/2021
* Main Resources used: none
*/

BinaryTree::BinaryTree()
{}

BinaryTree::~BinaryTree()
{
	delete mRoot; // deletes the root
}

void BinaryTree::Insert(int aValue) 
{
	BTreeNode* newnode = new BTreeNode();
	newnode->value = aValue;
	if (mRoot == nullptr) // if the root is null then it will insert to the root
	{
		mRoot = newnode;
	}
	else
	{
		Insert(mRoot, newnode); // passes the root and the newnode with the new value to be inserted
	}
}

bool BinaryTree::Contains(int aValue)
{
	try
	{
		if (search(mRoot, aValue) != nullptr) // checks if the search isnt null this is done because search throws if it cant find the node
		{
			return true;
		}
	}
	catch (const std::exception&)
	{
		return false; // the search threw and didnt find the node
	}
}

void BinaryTree::InOrderPrint()
{
	InOrder(mRoot); // for the recursive function
}

void BinaryTree::InOrder(BTreeNode* currentNode)
{
	if (currentNode == nullptr) // the end of the node chain is found
	{
		return;
	}
	else
	{
		InOrder(currentNode->left); // digs to the left as far as it can
		std::cout << currentNode->value << ", "; // prints the node value
		InOrder(currentNode->right); // digs to the right
	}
}

void BinaryTree::Insert(BTreeNode*& currentNode, BTreeNode*& newnode)
{
	if (newnode->value < currentNode->value) // checks if the value is to the left of the current node
	{
		if (currentNode->left == nullptr) // if its to the left and the left is null then it replaces the left with the new node
		{
			currentNode->left = newnode;
		}
		else
		{
			Insert(currentNode->left, newnode); // digs to the left
		}
	}
	else //the value should be to the right
	{
		if (currentNode->right == nullptr)// if its to the right and the right is null then it replaces the right with the new node
		{
			currentNode->right = newnode;
		}
		else
		{
			Insert(currentNode->right, newnode); // digs to the right
		}
	}
}

BinaryTree::BTreeNode*& BinaryTree::search(BTreeNode*& currentNode, int value)
{
	if (currentNode == nullptr)
	{
		throw std::invalid_argument("No Node Found"); // throws because the node isnt found 
	}
	else
	{
		if (currentNode->value == value) // returns the node when found
		{
			return currentNode;
		}
	}
	if (currentNode->value > value) // checks if its to the left or right
	{
		search(currentNode->left, value); // digs to the left
	}
	else
	{
		search(currentNode->right, value); // digs to the right
	}
}