#pragma once
#include <iostream>
#include <exception>

/*
* File name: BinaryTree
* FUNCTION: This is the sort binary tree this tree does not self balance and will only insert where the node is fit to go
* This tree can insert, look up nodes and print inorder
* Authour: Kaven Arango
* Date: 03/31/2021
* Main Resources used: none
*/



class BinaryTree
{
public:
    BinaryTree();

    // deleted the memory used by the struct
    ~BinaryTree();

    // insterts the root node or calls the recusive funtion
    void Insert(int aValue);

    // calls the recursive function
    bool Contains(int aValue);

    // Print all of the elements in the Binary Tree, in order
    void InOrderPrint();


private:

    struct BTreeNode
    {
        BTreeNode* left = nullptr;
        BTreeNode* right = nullptr;
        int value;
        ~BTreeNode()
        {
            delete left; // deletes everything to the left first
            delete right; // deletes everything to the right next
        }
    };

    void InOrder(BTreeNode* currentNode); // used for recursion
    void Insert(BTreeNode*& currentNode, BTreeNode*& aValue);// used for recursion
    BTreeNode*& search(BTreeNode*& currentNode, int value);// used for recursion
    BTreeNode* mRoot = nullptr; // the root node
};

