#include <iostream>
#include "BinaryTree.h"
/*
* File name: Source
* FUNCTION: This is the used to interact with the binary tree through the console where you can insert a new node look for a node and print inorder
* Authour: Kaven Arango
* Date: 03/31/2021
* Main Resources used: none
*/

using namespace std;



int main()
{
	BinaryTree* tree = new BinaryTree();
	bool quit = false;
	int choice; // if the user wants to insert look print or exit the program
	int insert; // if the user wants to insert or look the value they want to use
	while (false == quit)
	{
		cout << "1) Insert a node" << endl;
		cout << "2) Look for a node" << endl;
		cout << "3) print InOrder" << endl;
		cout << "4) Exit" << endl;
		cin >> choice;

		switch (choice)
		{
		case 1:
		{
			cout << "What do you want to insert" << endl;
			cin >> insert;
			tree->Insert(insert);
			break;
		}
		case 2:
		{
			cout << "What do you want to Look for" << endl;
			cin >> insert;
			if (tree->Contains(insert) == true)
			{
				cout << "This node exists" << endl;
			}
			else
			{
				cout << "This node does not exist" << endl;
			}
			break;
		}
		case 3:
		{
			tree->InOrderPrint();
			cout << endl;
			break;
		}
		case 4:
		{
			quit = true;
			break;
		}

		default:
		{
			cin.clear();
			cin.ignore();
			choice = 0;
			break;
		}
		}
	}
	delete tree;
	cout << "Have a good day" << endl;
	return 0;
}